﻿using System;
using System.Collections.Generic;

namespace TabataTimer.Data
{
	enum Phase
	{
		Preparation,
		Workout,
		Rest,
		Break
	}

	public class TimerCounter
	{
		private TimerParams timerParams;
		private Phase phase = Phase.Preparation;

		public uint Sets { get; private set; }

		public uint Cycles { get; private set; }

		public uint TotalRemainTime { get; private set; }

		public bool IsInProcess { get; private set; }

		public int PhaseNameId { 
			get 
			{
				switch (phase) 
				{
					case Phase.Preparation:
						return Resource.String.preparation;
					case Phase.Workout:
						return Resource.String.workout;				
					case Phase.Rest:
						return Resource.String.rest;				
					case Phase.Break:
						return Resource.String.recovery;	
					default:
						return 0;
				}
			}
		}

		public int PhaseColorId { 
			get 
			{
				switch (phase) 
				{
					case Phase.Preparation:
						return Resource.Color.preparation;
					case Phase.Workout:
						return Resource.Color.workout;				
					case Phase.Rest:
						return Resource.Color.rest;				
					case Phase.Break:
						return Resource.Color.recovery;	
					default:
						return 0;
				}
			}
		}

		public uint PhaseTime { 
			get 
			{
				switch (phase) 
				{
					case Phase.Preparation:
						return timerParams.PreparationTime;
					case Phase.Workout:
						return timerParams.WorkoutTime;				
					case Phase.Rest:
						return timerParams.RestTime;				
					case Phase.Break:
						return timerParams.BreakTime;	
					default:
						return 0;
				}
			}
		}

		public Queue<String> PhaseSounds { get; private set; }

		public TimerCounter (TimerParams timerParams)
		{
			this.timerParams = timerParams;
			PhaseSounds = new Queue<String> ();
			Reset ();
		}

		public void Reset ()
		{
			Sets = timerParams.Sets;
			Cycles = timerParams.Cycles;
			TotalRemainTime = timerParams.TotalTime;
			phase = timerParams.PreparationTime == 0 ? Phase.Workout : Phase.Preparation;
			IsInProcess = false;
			PhaseSounds.Clear ();
		}

		public void Tick()
		{
			TotalRemainTime--;
			IsInProcess = true;
		}

		public void NextPhase()
		{
			PhaseSounds.Clear ();

			switch (phase) 
			{
				case Phase.Preparation:
					phase = Phase.Workout;
					PhaseSounds.Enqueue("workout");
					break;
				case Phase.Workout:
					phase = Phase.Rest;
					PhaseSounds.Enqueue("rest");
					break;
				case Phase.Rest:
					Sets--;
					if (Sets != 0) 
					{
						phase = Phase.Workout;
						PhaseSounds.Enqueue("n" + Sets);
						PhaseSounds.Enqueue(Sets == 1 ? "togos2" : "togop2");
						PhaseSounds.Enqueue("workout");
					} 
					else 
					{
						Cycles--;
						if (Cycles != 0) 
						{
							phase = Phase.Break;
							PhaseSounds.Enqueue("repetitioncompleted");
						} 
						else 
						{
							Reset ();
						}
					}
					break;
				case Phase.Break:
						phase = Phase.Workout;
						Sets = timerParams.Sets;
						PhaseSounds.Enqueue("workout");
						break;
			}
		}
	}
}

