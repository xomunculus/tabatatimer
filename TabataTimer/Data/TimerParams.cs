﻿using System;

namespace TabataTimer.Data
{
	public class TimerParams
	{
		public uint Sets = Constants.DefaultSets;
		public uint Cycles = Constants.DefaultCycles;
		public uint WorkoutTime = Constants.DefaultWorkoutTime;
		public uint RestTime = Constants.DefaultRestTime;
		public uint BreakTime = Constants.DefaultBreakTime;
		public uint PreparationTime = Constants.DefaultPreparationTime;

		public uint TotalTime
		{
			get { return PreparationTime + (WorkoutTime + RestTime) * Sets * Cycles + (Cycles - 1) * BreakTime;}
		}

		public TimerParams ()
		{
		}
	}
}

