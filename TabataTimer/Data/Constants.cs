﻿using System;

namespace TabataTimer.Data
{
	public static class Constants
	{
		public static uint DefaultSets = 8;
		public static uint DefaultCycles = 1;
		public static uint DefaultWorkoutTime = 20;
		public static uint DefaultRestTime = 10;
		public static uint DefaultPreparationTime = 5;
		public static uint DefaultBreakTime = 30;
	}
}

