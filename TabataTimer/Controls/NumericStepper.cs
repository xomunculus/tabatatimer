﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.Res;

namespace TabataTimer.Controls
{
	public delegate void ValueChangedEventHandler (int oldVal, int newVal);
	public delegate string FormatHandler (int value);

	public class NumericStepper : LinearLayout
	{
		private TextView keyTextView;
		//StepperKey
		private TextView valueTextView;
		//StepperValue
		private Button decrementButton;
		private Button incrementButton;

		public event ValueChangedEventHandler ValueChanged;

		public FormatHandler Formater;

		private int maxValue;

		public int MaxValue {
			get { return maxValue; }
			set {
				if (value == maxValue)
					return;

				if (value >= minValue) {
					maxValue = value;
					CheckValue ();
					UpdateView ();
				}
			}
		}

		private int minValue;

		public int MinValue {
			get { return minValue; }
			set {
				if (value == minValue)
					return;

				if (value <= maxValue) {
					minValue = value;
					CheckValue ();
					UpdateView ();
				}
			}
		}

		private int value;

		public int Value {
			get { return value; }
			set {
				if (value == this.value)
					return;

				int oldValue = this.value;
				this.value = value;

				CheckValue ();
				ValueChanged (oldValue, this.value);
				UpdateView ();
			}
		}

		public string KeyName {
			get { 
				return  keyTextView != null ? keyTextView.Text : String.Empty;
			}
			set {
				if (keyTextView != null)
					keyTextView.Text = value;
			}
		}

		// Default constructor override
		public NumericStepper (Context context) : this (context, null)
		{
		}

		// Default constructor when inflating from XML file
		public NumericStepper (Context context, IAttributeSet attrs) : this (context, attrs, 0)
		{
		}

		// Default constructor override
		public NumericStepper (Context context, IAttributeSet attrs, int defStyle) : base (context, attrs, defStyle)
		{
			//Orientation = Orientation.Horizontal;

			LayoutInflater inflater = (LayoutInflater)Context.GetSystemService (Context.LayoutInflaterService);
			inflater.Inflate (Resource.Layout.NumericStepper, this, true);

			keyTextView = FindViewById<TextView> (Resource.Id.StepperKey);
			valueTextView = FindViewById<TextView> (Resource.Id.StepperValue);

			decrementButton = FindViewById<Button> (Resource.Id.DecrementButton);
			decrementButton.Click += (sender, e) => { Value--; };
			incrementButton = FindViewById<Button> (Resource.Id.IncrementButton);
			incrementButton.Click += (sender, e) => { Value++; };

			TypedArray typeArray = context.ObtainStyledAttributes (attrs, Resource.Styleable.NumericStepper, 0, 0);

			KeyName = typeArray.GetString (Resource.Styleable.NumericStepper_keyName);
			MaxValue = typeArray.GetInt (Resource.Styleable.NumericStepper_maxValue, 100);
			MinValue = typeArray.GetInt (Resource.Styleable.NumericStepper_minValue, 0);
			Value = typeArray.GetInt (Resource.Styleable.NumericStepper_value, 0);
		}

		private void CheckValue()
		{
			if (value > maxValue)
				value = maxValue;
			else if (value < minValue)
				value = minValue;
		}

		private void UpdateView ()
		{
			valueTextView.Text = Formater != null ? Formater (value) : value.ToString ();
			incrementButton.Enabled = value < maxValue;
			decrementButton.Enabled = value > minValue;
		}
	}
}

