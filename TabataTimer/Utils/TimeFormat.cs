﻿using System;

namespace TabataTimer
{
	public static class TimeFormat
	{
		public static String MillisecondsToString (double timeInMilliseconds)
		{
			return TimeSpanToString (TimeSpan.FromMilliseconds (timeInMilliseconds));
		}

		public static String SecondsToString (double timeInSeconds)
		{
			return TimeSpanToString (TimeSpan.FromSeconds (timeInSeconds));
		}

		public static String SecondsToString (int timeInSeconds)
		{
			return TimeSpanToString (TimeSpan.FromSeconds (timeInSeconds));
		}

		public static String TimeSpanToString (TimeSpan timespan)
		{
			if(timespan.Hours == 0)
				return string.Format ("{0:D2}:{1:D2}", timespan.Minutes, timespan.Seconds);
			else
				return string.Format ("{0:D2}:{0:D2}:{1:D2}", timespan.Hours, timespan.Minutes, timespan.Seconds);
		}
	}
}

