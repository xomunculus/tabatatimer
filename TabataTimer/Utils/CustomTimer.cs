﻿using System;
using System.Timers;
using System.ComponentModel;

namespace TabataTimer
{
	public class CustomTimer
	{
		private Timer timer;
		private uint repeatCount;

		public double Delay {
			get { return timer.Interval; }
			set {
				timer.Interval = value;
			}
		}

		public uint RepeatCount {
			get{ return repeatCount; }
			set {
				repeatCount = value; 
				if (CurrentCount >= repeatCount)
					Reset ();
			}
		}

		public uint CurrentCount {
			get;
			private set;
		}

		public uint RemainCount {
			get { return RepeatCount - CurrentCount; }
		}

		public bool IsRunning {
			get;
			private set;
		}

		/// <summary>
		/// Occurs when completed.
		/// </summary>
		public event Action Completed;

		/// <summary>
		/// Occurs when ticked.
		/// </summary>
		public event Action Ticked;

		public CustomTimer (double delay, uint repeatCount = uint.MaxValue)
		{
			timer = new Timer ();
			timer.Elapsed += OnTimedEvent;

			Delay = delay;
			RepeatCount = repeatCount;
		}

		public void Start ()
		{
			if (IsRunning == false) {
				IsRunning = true;

				timer.Start ();
			}
		}

		public void Stop ()
		{
			IsRunning = false;
			timer.Stop ();
		}

		public void Reset ()
		{
			Stop ();

			CurrentCount = 0;
		}

		private void OnTimedEvent (object sender, ElapsedEventArgs e)
		{
			CurrentCount++;

			Ticked ();

			if (CurrentCount == RepeatCount) {
				Stop ();
				Completed ();
			}
		}
	}
}

