﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using System;
using TabataTimer.Utils;
using Android.Util;
using TabataTimer.Data;
using Android.Content;
using Android.Media;
using Android.Content.Res;
using System.Collections.Generic;

namespace TabataTimer.Screens
{
	[Activity (Label = "TabataTimer", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private TextView phaseTextView;
		private TextView timerTextView;
		private TextView setsTextView;
		private TextView cyclesTextView;
		private TextView totalTimeTextView;
		private Button startButton;
		private Button stopButton;
		private Button pauseButton;
		private ViewGroup buttonsGroup;

		private CustomTimer timer;
		private TimerParams timerParams;
		private TimerCounter counter;
		private MediaPlayer player;
		private Queue<string> soundsToPlay;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.Main);

			phaseTextView = FindViewById<TextView> (Resource.Id.PhaseText);
			timerTextView = FindViewById<TextView> (Resource.Id.TimerText);
			setsTextView = FindViewById<TextView> (Resource.Id.SetsText);
			cyclesTextView = FindViewById<TextView> (Resource.Id.CyclesText);
			totalTimeTextView = FindViewById<TextView> (Resource.Id.TotalTimeText);

			startButton = FindViewById<Button> (Resource.Id.StartButton);
			startButton.Click += (sender, e) => StartTimer();
				
			stopButton = FindViewById<Button> (Resource.Id.StopButton);
			stopButton.Click += (sender, e) => StopTimer(true);

			pauseButton = FindViewById<Button> (Resource.Id.PauseButton);
			pauseButton.Click += (sender, e) => PauseResumeTimer();

			buttonsGroup = FindViewById<ViewGroup> (Resource.Id.ButtonsGroup);

			player = new MediaPlayer();
			player.Completion += (sender, e) => OnSoundCompleted();
			soundsToPlay = new Queue<string>();

			timerParams = new TimerParams ();
			counter = new TimerCounter (timerParams);

			timer = new CustomTimer (1000);
			timer.Ticked += OnTick;
			timer.Completed += OnCompleted;

			StopTimer ();
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Result.Ok) {
				Bundle bundle = data.Extras;
				if (bundle != null) {
					timerParams.Sets = (uint)bundle.GetInt ("sets", 0);
					timerParams.Cycles = (uint)bundle.GetInt ("cycles", 0);
					timerParams.PreparationTime = (uint)bundle.GetInt ("preparation", 0);
					timerParams.WorkoutTime = (uint)bundle.GetInt ("workout", 0);
					timerParams.RestTime = (uint)bundle.GetInt ("rest", 0);
					timerParams.BreakTime = (uint)bundle.GetInt ("break", 0);

					StopTimer ();
				}
			}
		}

		private void StartTimer ()
		{
			buttonsGroup.Visibility = ViewStates.Visible;
			startButton.Visibility = ViewStates.Gone;

			timer.Start ();
			PlaySound (Resource.Raw.timerStarted);
		}

		private void StopTimer (bool isForce = false)
		{
			buttonsGroup.Visibility = ViewStates.Gone;
			startButton.Visibility = ViewStates.Visible;
			pauseButton.Text = Resources.GetString(Resource.String.pause);

			timer.Reset ();
			counter.Reset ();
			ShowCounterPhase ();

			if(isForce)
				PlaySound (Resource.Raw.timerStopped);
		}

		private void PauseResumeTimer ()
		{
			if (timer.IsRunning) 
			{
				timer.Stop ();
				pauseButton.Text = Resources.GetString(Resource.String.resume);

				PlaySound (Resource.Raw.timerPaused);
			} 
			else 
			{
				timer.Start ();
				pauseButton.Text = Resources.GetString(Resource.String.pause);

				PlaySound (Resource.Raw.timerResumed);
			}
		}

		private void ShowCounterPhase()
		{
			phaseTextView.Text = Resources.GetString(counter.PhaseNameId);
			phaseTextView.SetTextColor (Resources.GetColor(counter.PhaseColorId));
			timerTextView.SetTextColor (Resources.GetColor(counter.PhaseColorId));
			timerTextView.Text = TimeFormat.SecondsToString (counter.PhaseTime);
			totalTimeTextView.Text = TimeFormat.SecondsToString (counter.TotalRemainTime);
			setsTextView.Text = counter.Sets.ToString();
			cyclesTextView.Text = counter.Cycles.ToString();
			timer.RepeatCount = counter.PhaseTime;
		}

		private void OnTick()
		{
			counter.Tick ();
			
			RunOnUiThread(() => { 
				timerTextView.Text = TimeFormat.SecondsToString (timer.RemainCount); 
				totalTimeTextView.Text = TimeFormat.SecondsToString (counter.TotalRemainTime);
			});

			if (timer.RemainCount < 4 && timer.RemainCount > 0) {
				PlaySound (Resources.GetIdentifier("n" + timer.RemainCount, "raw", PackageName));
			}
		}

		private void OnCompleted()
		{
			counter.NextPhase ();

			RunOnUiThread(() => { 
				if(counter.IsInProcess)
				{
					ShowCounterPhase();
					PlaySoundSequence(counter.PhaseSounds);
					timer.Reset(); 
					timer.Start();
				}
				else
				{
					StopTimer();
					PlaySound(Resource.Raw.timerCompleted);
				}
			});
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.MainMenu, menu);
			return true;
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			StopTimer ();

			Bundle bundle = new Bundle ();
			bundle.PutInt("sets", (int)timerParams.Sets);
			bundle.PutInt("cycles", (int)timerParams.Cycles);
			bundle.PutInt("workout", (int)timerParams.WorkoutTime);
			bundle.PutInt("rest", (int)timerParams.RestTime);
			bundle.PutInt("preparation", (int)timerParams.PreparationTime);
			bundle.PutInt ("break", (int)timerParams.BreakTime);

			var intent = new Intent(this, typeof(EditActivity));
			intent.PutExtras (bundle);
			StartActivityForResult(intent, 0);

			return base.OnOptionsItemSelected(item);
		}

		private void PlaySound(int resId)
		{
			if (player.IsPlaying)
				return;

			AssetFileDescriptor afd = Resources.OpenRawResourceFd(resId);

			player.Reset ();
			player.SetDataSource (afd.FileDescriptor, afd.StartOffset, afd.Length);
			player.Prepare();
			player.Start();

			afd.Close ();
		}

		private void PlaySoundSequence(Queue<string> queue)
		{
			if (queue.Count > 0) 
			{
				soundsToPlay = queue;
				OnSoundCompleted ();
			}
		}

		public void OnSoundCompleted ()
		{
			if (soundsToPlay.Count > 0) 
			{
				string resName = soundsToPlay.Dequeue ();
				PlaySound (Resources.GetIdentifier(resName, "raw", PackageName));
			}
		}

		protected override void OnDestroy ()
		{
			player.Release ();

			base.OnDestroy ();
		}
	}
}


