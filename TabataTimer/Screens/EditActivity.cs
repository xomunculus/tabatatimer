﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TabataTimer.Data;
using TabataTimer.Controls;

namespace TabataTimer.Screens
{
	[Activity (Label = "@string/edit")]			
	public class EditActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.Edit);

			Bundle bundle = Intent.Extras;

			NumericStepper setsStepper = FindViewById<NumericStepper> (Resource.Id.SetsStepper);
			setsStepper.Value = bundle.GetInt ("sets", 0);

			NumericStepper cyclesStepper = FindViewById<NumericStepper> (Resource.Id.CyclesStepper);
			cyclesStepper.Value = bundle.GetInt ("cycles", 0);

			NumericStepper preparationStepper = FindViewById<NumericStepper> (Resource.Id.PreparationStepper);
			preparationStepper.Formater = TimeFormat.SecondsToString;
			preparationStepper.Value = bundle.GetInt ("preparation", 0);

			NumericStepper workoutStepper = FindViewById<NumericStepper> (Resource.Id.WorkoutStepper);
			workoutStepper.Formater = TimeFormat.SecondsToString;
			workoutStepper.Value = bundle.GetInt ("workout", 0);

			NumericStepper restStepper = FindViewById<NumericStepper> (Resource.Id.RestStepper);
			restStepper.Formater = TimeFormat.SecondsToString;
			restStepper.Value = bundle.GetInt ("rest", 0);

			NumericStepper breakStepper = FindViewById<NumericStepper> (Resource.Id.BreakStepper);
			breakStepper.Formater = TimeFormat.SecondsToString;
			breakStepper.Value = bundle.GetInt ("break", 0);

			Button cancelButton = FindViewById<Button> (Resource.Id.CancelButton);
			cancelButton.Click += (sender, e) => Finish();

			Button saveButton = FindViewById<Button> (Resource.Id.SaveButton);
			saveButton.Click += (sender, e) => {
				bundle = new Bundle();
				bundle.PutInt("sets", setsStepper.Value);
				bundle.PutInt("cycles", cyclesStepper.Value);
				bundle.PutInt("workout", workoutStepper.Value);
				bundle.PutInt("rest", restStepper.Value);
				bundle.PutInt("preparation", preparationStepper.Value);
				bundle.PutInt ("break", breakStepper.Value);

				Intent mIntent = new Intent();
				mIntent.PutExtras(bundle);
				SetResult(Result.Ok, mIntent);
				Finish();
			};
		}
	}
}

